﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguage
{/// <summary>
///  Factory Desigin pattern is used to create object without exposing the creation logic and refer to newly created object using a common interface.
/// </summary>
    class ShapeFactory
    {
        /// <summary>
        /// Gets the type of shape and returns its object
        /// </summary>
        /// <param name="shapeType"> String.</param>
        /// <returns>Shape object</returns>
        public Shape GetShape(String shapeType)
        {
            shapeType = shapeType.ToUpper().Trim(); //yoi could argue that you want a specific word string to create an object but I'm allowing any case combination


            if (shapeType.Equals("CIRCLE"))
            {
                return new Circle();

            }
            else if (shapeType.Equals("RECTANGLE"))
            {
                return new Rectangle();

            }
            else if (shapeType.Equals("TRIANGLE"))
            {
                return new Triangle();
            }
            else if (shapeType.Equals("POLYGON"))
            {
                return new Polygon();
            }
            else
            {
                //if we get here then what has been passed in is unknown so throw an appropriate exception
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }

        }
    }
}
