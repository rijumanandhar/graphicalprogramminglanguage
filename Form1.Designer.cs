﻿namespace GraphicalProgrammingLanguage
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlDraw = new System.Windows.Forms.Panel();
            this.txtSingle = new System.Windows.Forms.TextBox();
            this.btnRun = new System.Windows.Forms.Button();
            this.txtMultiple = new System.Windows.Forms.TextBox();
            this.btnExecute = new System.Windows.Forms.Button();
            this.btnClearPnl = new System.Windows.Forms.Button();
            this.output = new System.Windows.Forms.Label();
            this.btnClearRun = new System.Windows.Forms.Button();
            this.btnClearExt = new System.Windows.Forms.Button();
            this.btnPen = new System.Windows.Forms.Button();
            this.btnColor = new System.Windows.Forms.Button();
            this.btnCanvas = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(936, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // pnlDraw
            // 
            this.pnlDraw.Location = new System.Drawing.Point(44, 56);
            this.pnlDraw.Name = "pnlDraw";
            this.pnlDraw.Size = new System.Drawing.Size(456, 595);
            this.pnlDraw.TabIndex = 2;
            this.pnlDraw.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlDraw_Paint);
            // 
            // txtSingle
            // 
            this.txtSingle.Location = new System.Drawing.Point(561, 92);
            this.txtSingle.Name = "txtSingle";
            this.txtSingle.Size = new System.Drawing.Size(321, 20);
            this.txtSingle.TabIndex = 3;
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(606, 130);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 4;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // txtMultiple
            // 
            this.txtMultiple.Location = new System.Drawing.Point(561, 182);
            this.txtMultiple.Multiline = true;
            this.txtMultiple.Name = "txtMultiple";
            this.txtMultiple.Size = new System.Drawing.Size(321, 350);
            this.txtMultiple.TabIndex = 5;
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(606, 558);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(75, 23);
            this.btnExecute.TabIndex = 6;
            this.btnExecute.Text = "Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // btnClearPnl
            // 
            this.btnClearPnl.Location = new System.Drawing.Point(561, 56);
            this.btnClearPnl.Name = "btnClearPnl";
            this.btnClearPnl.Size = new System.Drawing.Size(75, 23);
            this.btnClearPnl.TabIndex = 7;
            this.btnClearPnl.Text = "Clear Panel";
            this.btnClearPnl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClearPnl.UseVisualStyleBackColor = true;
            this.btnClearPnl.Click += new System.EventHandler(this.btnClear1_Click);
            // 
            // output
            // 
            this.output.AutoSize = true;
            this.output.Location = new System.Drawing.Point(558, 602);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(0, 13);
            this.output.TabIndex = 9;
            // 
            // btnClearRun
            // 
            this.btnClearRun.Location = new System.Drawing.Point(741, 130);
            this.btnClearRun.Name = "btnClearRun";
            this.btnClearRun.Size = new System.Drawing.Size(75, 23);
            this.btnClearRun.TabIndex = 11;
            this.btnClearRun.Text = "Clear";
            this.btnClearRun.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClearRun.UseVisualStyleBackColor = true;
            this.btnClearRun.Click += new System.EventHandler(this.btnClearRun_Click);
            // 
            // btnClearExt
            // 
            this.btnClearExt.Location = new System.Drawing.Point(741, 558);
            this.btnClearExt.Name = "btnClearExt";
            this.btnClearExt.Size = new System.Drawing.Size(75, 23);
            this.btnClearExt.TabIndex = 12;
            this.btnClearExt.Text = "Clear All";
            this.btnClearExt.UseVisualStyleBackColor = true;
            this.btnClearExt.Click += new System.EventHandler(this.btnClearExt_Click);
            // 
            // btnPen
            // 
            this.btnPen.Location = new System.Drawing.Point(705, 55);
            this.btnPen.Name = "btnPen";
            this.btnPen.Size = new System.Drawing.Size(55, 23);
            this.btnPen.TabIndex = 14;
            this.btnPen.Text = "Pen";
            this.btnPen.UseVisualStyleBackColor = true;
            this.btnPen.Click += new System.EventHandler(this.btnPen_Click);
            // 
            // btnColor
            // 
            this.btnColor.Location = new System.Drawing.Point(766, 55);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(55, 23);
            this.btnColor.TabIndex = 14;
            this.btnColor.Text = "Fill";
            this.btnColor.UseVisualStyleBackColor = true;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnCanvas
            // 
            this.btnCanvas.Location = new System.Drawing.Point(827, 56);
            this.btnCanvas.Name = "btnCanvas";
            this.btnCanvas.Size = new System.Drawing.Size(55, 23);
            this.btnCanvas.TabIndex = 15;
            this.btnCanvas.Text = "Canvas";
            this.btnCanvas.UseVisualStyleBackColor = true;
            this.btnCanvas.Click += new System.EventHandler(this.btnCanvas_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(642, 55);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(60, 23);
            this.btnReset.TabIndex = 16;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 694);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnCanvas);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.btnPen);
            this.Controls.Add(this.btnClearExt);
            this.Controls.Add(this.btnClearRun);
            this.Controls.Add(this.output);
            this.Controls.Add(this.btnClearPnl);
            this.Controls.Add(this.btnExecute);
            this.Controls.Add(this.txtMultiple);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.txtSingle);
            this.Controls.Add(this.pnlDraw);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Graphical Programming Language";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Panel pnlDraw;
        private System.Windows.Forms.TextBox txtSingle;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.TextBox txtMultiple;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.Button btnClearPnl;
        private System.Windows.Forms.Label output;
        private System.Windows.Forms.Button btnClearRun;
        private System.Windows.Forms.Button btnClearExt;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btnPen;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.Button btnCanvas;
        private System.Windows.Forms.Button btnReset;
    }
}

