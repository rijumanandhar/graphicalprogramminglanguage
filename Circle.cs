﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguage
{
    /// <summary>
    /// Sub class of Shape class.
    /// </summary>
    class Circle : Shape
    {
        /// <summary>
        /// Radius of the circle
        /// </summary>
        private int radius;

        /// <summary>
        /// This method overrides Shape class's set method. It sets the value of radius.
        /// </summary>
        /// <param name="list">Takes a variable number of arguments.</param>
        public override void set(params int[] list)
        {
            radius = list[0];
        }

        /// <summary>
        /// This method overrides Shape class's draw method. It sets the value of radius.
        /// </summary>
        /// <param name="g">Graphics object to draw and display line</param>
        /// <param name="penColor">Color of pen</param>
        /// <param name="fillColor">Color of fill</param>
        public override void draw(Graphics g, Color penColor, Color fillColor)
        {
            Pen p = new Pen(penColor, 2);
            SolidBrush sb = new SolidBrush(fillColor);
            g.FillEllipse(sb, xCord, yCord, radius, radius);
            g.DrawEllipse(p, xCord, yCord, radius, radius);
        }
    }
}
