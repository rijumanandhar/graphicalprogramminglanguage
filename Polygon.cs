﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguage
{
    class Polygon: Shape
    {
        /// <summary>
        /// Points of polygon
        /// </summary>
        Point[] points;

        /// <summary>
        /// This method overrides Shape class's set method. It sets the value of radius.
        /// </summary>
        /// <param name="list">Takes a variable number of arguments.</param>
        public override void set(params int[] list)
        {
 
        }

        /// <summary>
        /// This method overrides Shape class's setPoints method. It sets the value of points.
        /// </summary>
        /// <param name="points">Points of polygon</param>
        public override void setPoints(Point[] points)
        {
            this.points = points;
        }

        /// <summary>
        /// This method overrides Shape class's draw method. It sets the value of points.
        /// </summary>
        /// <param name="g">Graphics object to draw and display line</param>
        /// <param name="penColor">Color of pen</param>
        /// <param name="fillColor">Color of fill</param>
        public override void draw(Graphics g, Color penColor, Color fillColor)
        {
            Pen p = new Pen(penColor, 2);
            SolidBrush sb = new SolidBrush(fillColor);
            g.DrawPolygon(p, points);
            g.FillPolygon(sb, points);
        }
    }
}
