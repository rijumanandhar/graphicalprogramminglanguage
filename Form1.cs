﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguage
{
    /// <summary>
    /// This class contains the interface with textbox for typing program into and a panel for displaying output
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// valid commands stored in an array
        /// </summary>
        private String[] commands = { "drawto", "moveto", "circle", "rectangle", "triangle", "polygon"};

        private String[] reserve = { "dec-var", "if-true", "if-false", "mul-if", "var-update", "if-end","then","error","complete","ignored", "method-dec", "method-end", "false-if" };

        CommandLineParser cl = new CommandLineParser();

        /// <summary>
        /// x and y coordinates
        /// </summary>
        int x = 0, y = 0;


        ShapeFactory shapeFactory = new ShapeFactory();
        Graphics g;
        bool save = false;
        String file = null;
        String filename = null;
        String result;

        /// <summary>
        /// Initialize form components
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            g = pnlDraw.CreateGraphics();
            btnPen.BackColor = Color.Black;
            btnColor.BackColor = Color.White;
            btnCanvas.BackColor = Color.White;
            pnlDraw.BackColor = btnCanvas.BackColor;
        }

        /// <summary>
        /// Clears the drawing panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear1_Click(object sender, EventArgs e)
        {
            pnlDraw.Refresh();
            output.Text ="Refresh";
            pnlDraw.BackColor = btnCanvas.BackColor;
        }

        /// <summary>
        /// Runs single line command parser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRun_Click(object sender, EventArgs e)
        {
            CommandLineParser cl = new CommandLineParser();
            String commandIn = txtSingle.Text;
            result = cl.singleLineReader(commandIn);
            executeCommand(result);
        }

        private void pnlDraw_Paint(object sender, PaintEventArgs e)
        {

        }

        /// <summary>
        /// Runs multiple line command parser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExecute_Click(object sender, EventArgs e)
        {
            String mulCommandIn = txtMultiple.Text;
            runExecuteButton(mulCommandIn);
        }

        public void runExecuteButton(String mulCommandIn)
        {
            cl.methodDict.Clear();
            cl.varDict.Clear();
            String[] inputs = mulCommandIn.Split('\n');
            String[] checker = new String[inputs.Length];
            int checkCount = 0;
            foreach (String lines in inputs)
            {
                checker[checkCount] = cl.checkSyntax(lines);
                Console.WriteLine(checkCount + ". " + checker[checkCount]);
                checkCount++;
            }

            for (int i = 0; i < checker.Length; i++)
            {
                String[] CheckArray = checker[i].Split();
                if (checker[i] == "if-true" && checker.Contains("mul-if"))
                {
                    int index = Array.IndexOf(checker, "mul-if");
                    if (index > i)
                    {
                        for (int j = i + 1; j <= index; j++)
                        {
                            if (checker[j] != "error")
                            {
                                executeCommand(checker[j]);
                                checker[j] = "complete";
                            }
                        }
                    }
                }
                else if (checker[i] == "if-false" && checker.Contains("mul-if"))
                {
                    int index = Array.IndexOf(checker, "mul-if");
                    if (index > i)
                    {
                        for (int j = i + 1; j <= index; j++)
                        {
                            if (checker[j] != "error")
                            {
                                checker[j] = "ignored";
                            }
                        }

                    }
                }
                else if ((checker[i] == "if-false" || checker[i] == "if-true") && !checker.Contains("mul-if"))
                {
                    MessageBox.Show("If without end"
                           , "If error");
                }
                else if (CheckArray[0] == "loop-true")
                {
                    if (checker.Contains("end-loop"))
                    {
                        int index = Array.IndexOf(checker, "end-loop");
                        if (index > i)
                        {
                            int limit = cl.LoopLimit;
                            Console.WriteLine("index: " + index + " i: " + i + " limit: " + limit);
                            int k = System.Convert.ToInt32(CheckArray[3]);
                            Console.WriteLine("k: " + k);
                            if (CheckArray[1] == "<=")
                            {
                                while (k <= limit)
                                {
                                    for (int j = i + 1; j < index; j++)
                                    {
                                        if (checker[j] != "error")
                                        {
                                            String[] CheckArray2 = checker[j].Split();
                                            Console.WriteLine(k + ":k j:" + j);
                                            Console.WriteLine("check20: " + CheckArray2[0]);
                                            if (CheckArray2[0] == "var-update" && CheckArray2[1] == cl.LoopLimitkey)
                                            {
                                                checker[j] = cl.checkSyntax(inputs[j]);
                                                CheckArray2 = checker[j].Split();
                                                Console.WriteLine(checker[j]);
                                                k = System.Convert.ToInt32(CheckArray2[3]);
                                                Console.WriteLine("k: " + k);
                                            }
                                            else
                                            {
                                                checker[j] = cl.checkSyntax(inputs[j]);
                                                Console.WriteLine(checker[j]);
                                                executeCommand(checker[j]);
                                            }
                                        }
                                    }
                                }
                                for (int j = i + 1; j < index; j++)
                                {
                                    checker[j] = "complete";
                                }
                            }
                            else if (CheckArray[1] == ">=")
                            {
                                while (k >= limit)
                                {
                                    for (int l= i + 1; l < index; l++)
                                    {
                                        Console.WriteLine(checker[l]);
                                        Console.WriteLine("l: " + l);
                                        if (checker[l] != "error")
                                        {
                                            String[] CheckArray2 = checker[l].Split();
                                            Console.WriteLine(k + ":k j:" + l);
                                            Console.WriteLine("check20: " + CheckArray2[0]);
                                            if (CheckArray2[0] == "var-update" && CheckArray2[1] == cl.LoopLimitkey)
                                            {
                                                checker[l] = cl.checkSyntax(inputs[l]);
                                                CheckArray2 = checker[l].Split();
                                                Console.WriteLine(checker[l]);
                                                k = System.Convert.ToInt32(CheckArray2[3]);
                                                Console.WriteLine("k: " + k);
                                            }
                                            else
                                            {
                                                checker[l] = cl.checkSyntax(inputs[l]);
                                                Console.WriteLine(checker[l]);
                                                executeCommand(checker[l]);
                                            }
                                            //executeCommand(cl.checkSyntax(inputs[j]));
                                            Console.WriteLine();
                                        }
                                    }
                                }
                                for (int j = i + 1; j < index; j++)
                                {
                                    checker[j] = "complete";
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Loop without end" +
                            "loop", "Loop error");
                    }
                }
                else if (checker[i] == "loop-false" && checker.Contains("end-loop"))
                {
                    int index = Array.IndexOf(checker, "end-loop");
                    if (index > i)
                    {
                        for (int j = i + 1; j < index; j++)
                        {
                            checker[j] = "ignored";
                        }
                    }
                }
                else if (CheckArray[0] == "method-dec" && checker.Contains("method-end"))
                {
                    String methodName = CheckArray[1];
                    String methodBody = cl.methodDict[methodName];
                    int index = Array.IndexOf(checker, "method-end");
                    if (index > i)
                    {
                        for (int j = i + 1; j < index; j++)
                        {
                            methodBody = methodBody + "\n" + inputs[j];
                            checker[j] = "complete";
                        }
                        cl.methodDict[methodName] = methodBody;
                    }
                }
                else if (CheckArray[0] == "method-dec" && !checker.Contains("method-end"))
                {
                    MessageBox.Show("Method without endmethod", "Method error");
                }
                else if (CheckArray[0] == "method-call" && cl.methodDict.ContainsKey(CheckArray[1]))
                {
                    methodReader(CheckArray[1], CheckArray[2]);
                }
                else if (!reserve.Contains(checker[i]) && !reserve.Contains(CheckArray[0]))
                {
                    string s = checker[i];
                    executeCommand(s);
                }

            }
            output.Text = checkCount + " Statement(s) executed";
        }

        public void methodReader(String methodname, String parameters)
        {
            String methodbody = cl.methodDict[methodname];
            String[] methodBodyLine = methodbody.Split('\n');
            String[] parameterName = methodBodyLine[0].Split(',');
            String[] parameterValue = parameters.Split(',');
            //Console.WriteLine(parameterName.Length + ":name  value:"+ parameterValue.Length + " "+ parameterValue[0] + parameterName[0]);
            if (parameterName.Length == parameterValue.Length && (parameterValue[0]!="none" || parameterName[0]!="none"))
            {
                for (int i=0; i< parameterName.Length; i++)
                {
                    cl.varDict.Add(parameterName[i], System.Convert.ToInt32(parameterValue[i]));
                    Console.WriteLine(parameterName[i] + " = "+ cl.varDict[parameterName[i]]);
                }
                for (int i = 1; i < methodBodyLine.Length; i++)
                {
                    result = cl.singleLineReader(methodBodyLine[i]);
                    Console.WriteLine(result);
                    executeCommand(result);
                }
                for (int i = 0; i < parameterName.Length; i++)
                {
                    cl.varDict.Remove(parameterName[i]);
                    //Console.WriteLine(parameterName[i] + " = " + cl.varDict[parameterName[i]]);
                }
            }
            else
            {
                for (int i = 1; i < methodBodyLine.Length; i++)
                {
                    result = cl.singleLineReader(methodBodyLine[i]);
                    Console.WriteLine(result);
                    executeCommand(result);
                }
            }
        }

        /// <summary>
        /// Clears the content of single line command parser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearRun_Click(object sender, EventArgs e)
        {
            txtSingle.Text = "";
        }

        /// <summary>
        /// Clears the content of multiple line command parser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearExt_Click(object sender, EventArgs e)
        {
            txtMultiple.Text = "";
        }

        /// <summary>
        /// This method allows user to select the color of their pen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPen_Click(object sender, EventArgs e)
        {
            ColorDialog pen = new ColorDialog();
            if (pen.ShowDialog() == DialogResult.OK)
            {
                btnPen.BackColor = pen.Color;
            }
        }

        /// <summary>
        /// This method allows user to select the color of their fill
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnColor_Click(object sender, EventArgs e)
        {
            ColorDialog color = new ColorDialog();
            if (color.ShowDialog() == DialogResult.OK)
            {
                btnColor.BackColor = color.Color;
            }
        }

        /// <summary>
        /// This method allows user to select the color of their canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCanvas_Click(object sender, EventArgs e)
        {
            ColorDialog canvas = new ColorDialog();
            if (canvas.ShowDialog() == DialogResult.OK)
            {
                btnCanvas.BackColor = canvas.Color;
                pnlDraw.BackColor = btnCanvas.BackColor;
            }
        }

        /// <summary>
        /// This method allows user to exit out of the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!save)
            {
                string message = "Do you want to save before closing?";
                string title = "Close Window";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show(message, title, buttons);
                if (result == DialogResult.Yes)
                {
                   // saveFile();
                }
                else
                {
                    System.Windows.Forms.Application.Exit();
                }
            }
            else
            {
                System.Windows.Forms.Application.Exit();
            }
            
        }

        /// <summary>
        /// This method allows user to select a text file to open and load into the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.ShowDialog();
                filename = openFileDialog1.FileName;
                StreamReader fReader = File.OpenText(filename);
                string line;
                // Read and display lines from the file until the end of 
                // the file is reached.
                file = null;
                while ((line = fReader.ReadLine()) != null)
                {
                    file += line+"\r\n";
                }
                Console.WriteLine("open garda \n" + file + "\n");
                txtMultiple.Text = file;
                fReader.Close();
                output.Text = "File Opened";
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Error", "Cannot find text file");
            }
            catch (IOException ie)
            {
                MessageBox.Show("Error", "IO Exception");
            }
        }

        /// <summary>
        /// This method allows user to save the commands into a file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveAs();
        }

        /// <summary>
        /// This method saves commands into a text file by opening a save dialogue box
        /// </summary>
        public void saveAs()
        {
            
                SaveFileDialog SaveFileDialog1 = new SaveFileDialog();
                SaveFileDialog1.DefaultExt = "txt";
                SaveFileDialog1.Filter = "txt files (*.txt))|*.txt|All files (*.*)|*.*";
                SaveFileDialog1.FilterIndex = 2;
                SaveFileDialog1.CheckPathExists = true;
                if (SaveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        filename = SaveFileDialog1.FileName;
                        StreamWriter fWriter = File.CreateText(filename);
                        fWriter.WriteLine(txtMultiple.Text);
                        fWriter.Close();
                        save = true;
                        output.Text = "File Saved";
                    }
                    catch (FileNotFoundException)
                    {
                        MessageBox.Show("Error", "Cannot find text file");
                    }
                    catch (IOException ie)
                    {
                        MessageBox.Show("Error", "IO Exception");
                    }

                }
              
        }
        /// <summary>
        /// This method saves commands into a text file
        /// </summary>
        public void SaveFile()
        {
            if (filename != null)
            {
                try
                {
                    StreamWriter fWriter = File.CreateText(filename);
                    fWriter.WriteLine(txtMultiple.Text);
                    fWriter.Close();
                    output.Text = "File Saved";
                    save = true;
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("Error", "Cannot find text file");
                }
                catch (IOException ie)
                {
                    MessageBox.Show("Error", "IO Exception");
                }
            }
            else
            {
                saveAs();
            }
        }
        /// <summary>
        /// This method allows user to save files in text format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
        }
        /// <summary>
        /// This method resets the position of pen to 0,0
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReset_Click(object sender, EventArgs e)
        {
            x =0;
            y = 0;
            output.Text = "Pen Reset";
        }
        /// <summary>
        /// This method receives valid commands and executes them accordingly
        /// </summary>
        /// <remarks>
        /// This method untilizes ShapeFactory 
        /// </remarks>
        /// <param name="command"></param>
        public void executeCommand (string command)
        {
            //Console.WriteLine("Inside executecommand");
            string[] inputs = command.Trim().Split();
         
            if (inputs[0] == commands[0])
            {
                Shape s1 = new Shape();
                s1.move(x, y);
                s1.drawline(System.Convert.ToInt32(inputs[1]), System.Convert.ToInt32(inputs[2]), g, btnPen.BackColor);
                output.Text = "Draw to " + inputs[1] + "," + inputs[2];
            }
            else if (inputs[0] == commands[1])
            {
                x = System.Convert.ToInt32(inputs[1]);
                y = System.Convert.ToInt32(inputs[2]);
          
                output.Text = "Pen Move to " + inputs[1] + "," + inputs[2];
            }
            else if (inputs[0] == commands[2])
            {
                Shape circle = shapeFactory.GetShape("CIRCLE");
                circle.move(x, y);
                circle.set(Convert.ToInt32(inputs[1]));
                circle.draw(g, btnPen.BackColor, btnColor.BackColor);
                output.Text = "Drawing Circle. Radius size:" + inputs[1];
            }
            else if (inputs[0] == commands[3])
            {
                Shape rectangle = shapeFactory.GetShape("RECTANGLE");
                rectangle.move(x, y);
                rectangle.set(Convert.ToInt32(inputs[1]), Convert.ToInt32(inputs[2]));
                rectangle.draw(g, btnPen.BackColor, btnColor.BackColor);
                output.Text = "Drawing Rectangle. Width :" + inputs[1] + " Height:" + inputs[2];
            }
            else if (inputs[0] == commands[4])
            {
                Shape triangle = shapeFactory.GetShape("TRIANGLE");
                triangle.move(x, y);
                triangle.set(Convert.ToInt32(inputs[1]), Convert.ToInt32(inputs[2]), Convert.ToInt32(inputs[3]));
                triangle.draw(g, btnPen.BackColor, btnColor.BackColor);
                output.Text = "Drawing Triangle. Base:" + inputs[1] + " height:" + inputs[2] + " hyp:" + inputs[3];
            }
            else if (inputs[0] == commands[5])
            {
                int length = (inputs.Length - 1) * 2;
                Point[] points = new Point[length];
                Shape polygon = shapeFactory.GetShape("POLYGON");
                polygon.move(x, y);
                int j = 0;
                for (int i = 1; i < inputs.Length; i++)
                {
                    String[] parameters = inputs[i].Split(',');
                    points[j] = new Point(Convert.ToInt32(parameters[0]), Convert.ToInt32(parameters[1]));
                    j++;
                }
                polygon.setPoints(points);
                polygon.draw(g, btnPen.BackColor, btnColor.BackColor);
                output.Text = "Drawing Polygon.";
            }
        }
    }
}
