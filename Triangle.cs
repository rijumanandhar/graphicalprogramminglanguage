﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguage
{
    /// <summary>
    /// Sub class of Shape class.
    /// </summary>
    class Triangle : Shape
    {
        /// <summary>
        /// base, hypotenus and adjacent sides of a triangle.
        /// </summary>
        private int adj, bse, hyp;

        /// <summary>
        /// This method overrides Shape class's set method. It sets the value of hyp, bse and adj.
        /// </summary>
        /// <param name="list">Takes a variable number of arguments.</param>
        public override void set(params int[] list)
        {
            adj = list[0];
            bse = list[1];
            hyp = list[2];
        }

        /// <summary>
        /// This method overrides Shape class's draw method. It sets the value of hyp, bse and adj of the triangle.
        /// </summary>
        /// <param name="g">Graphics object to draw and display line</param>
        /// <param name="penColor">Color of pen</param>
        /// <param name="fillColor">Color of fill</param>
        public override void draw(Graphics g, Color penColor, Color fillColor)
        {
            Pen p = new Pen(penColor, 2);
            SolidBrush sb = new SolidBrush(fillColor);
            Point[] points = { new Point(xCord, yCord), new Point(xCord+bse, yCord), new Point(xCord + bse, yCord + adj) };
            g.DrawPolygon(p, points);
            g.FillPolygon(sb, points);
        }
    }
}
