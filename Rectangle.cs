﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguage
{
    /// <summary>
    /// Sub class of Shape class.
    /// </summary>
    class Rectangle : Shape
    {
        /// <summary>
        /// width and height of the rectangle.
        /// </summary>
        private int width, height;

        /// <summary>
        ///This method overrides Shape class's set method. It sets the value of width and height of the rectangle.
        /// </summary>
        /// <param name="list">Takes a variable number of arguments.</param>
        public override void set(params int[] list)
        {
            width = list[0];
            height = list[1];
        }

        /// <summary>
        /// This method overrides Shape class's draw method. It sets the value of width and height of the rectangle.
        /// </summary>
        /// <param name="g">Graphics object to draw and display line</param>
        /// <param name="penColor">Color of pen</param>
        /// <param name="fillColor">Color of fill</param>
        public override void draw(Graphics g, Color penColor, Color fillColor)
        {
            Pen p = new Pen(penColor, 2);
            SolidBrush sb = new SolidBrush(fillColor);
            g.FillRectangle(sb, xCord, yCord, width, height);
            g.DrawRectangle(p, xCord, yCord, width, height);
        }
    }
}
