﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguage
{
    /// <summary>
    /// Shape class is the parent of Circle, Triangle and Rectangle.
    /// These child classes inherit all properties of Shape class.
    /// </summary>
    /// <remarks>
    /// This class can move, draw shape, draw line and set parameters of shapes.
    /// </remarks>
    class Shape
    {
        //x and y coordinates from where shapes are drawn
        protected int xCord, yCord;

        /// <summary>
        /// Initialize the value of x and y coordinates to 0
        /// </summary>
        public Shape()
        {
            xCord = 0;
            yCord = 0;
        }

        /// <summary>
        /// Moves the position of x, y coordinates to desired spot
        /// </summary>
        /// <param name="x">Integer number</param>
        /// <param name="y">Integer number</param>
        public void move(int x, int y)
        {
            xCord = x;
            yCord = y;
        }

        /// <summary>
        /// Draws the shape based on given parameters.
        /// </summary>
        /// <remarks>
        /// This method is overridden by child classes to draw specific shapes.
        /// </remarks>
        /// <param name="g"> Graphics object to draw and display shapes</param>
        /// <param name="penColor"> Color of pen</param>
        /// <param name="fillColor"> Color of fill</param>
        public virtual void draw(Graphics g, Color penColor, Color fillColor)
        {
          
        }

        /// <summary>
        /// This function sets the parameter for the shape to be drawn.
        /// </summary>
        /// <remarks>
        /// This method is overridden by child classes to set specific parameters
        /// </remarks>
        /// <param name="list"> Takes list of parameters</param>
        public virtual void set(params int[] list)
        {
            
        }

        public virtual void setPoints(Point[] points)
        {

        }

        /// <summary>
        /// This method draws a line from initial x-y coordinates to coordinate passed in the parameter
        /// </summary>
        /// <param name="x">Integer Number.</param>
        /// <param name="y">Integer Number</param>
        /// <param name="g">Graphics object to draw and display line</param>
        /// <param name="penColor">Color of the pen</param>
        public void drawline(int x,int y, Graphics g, Color penColor)
        {
            Pen p = new Pen(penColor, 2);
            g.DrawLine(p, new Point(xCord, yCord), new Point(x, y));
        }

    }
}
