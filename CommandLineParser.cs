﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguage
{
    /// <summary>
    /// CommandLineParser reads and executes commands
    /// </summary>
    public class CommandLineParser
    {
        /// <summary>
        /// valid commands stored in an array
        /// </summary>
        private String[] commands = { "drawto", "moveto", "circle", "rectangle", "triangle", "polygon" };

        /// <summary>
        /// valid keywords stored in an array
        /// </summary>
        private String[] keywords = { "var", "loop", "endloop", "for", "if", "endif", "then","method","endmethod"};

        /// <summary>
        /// Stores name of variable as key and its value
        /// </summary>
        public Dictionary<string, int> varDict = new Dictionary<string, int>();

        public Dictionary<string, string> methodDict = new Dictionary<string, string>();

        private int looplimit = 0;
        private String looplimitkey = null;

        public int LoopLimit
        {
            get
            {
                return looplimit;
            }
            set
            {
                looplimit = value;
            }
        }

        public String LoopLimitkey
        {
            get
            {
                return looplimitkey;
            }
            set
            {
                looplimitkey = value;
            }
        }

        public bool ifcon= false, loop = false, method = false;

        /// <summary>
        /// This method checks the syntax
        /// </summary>
        public String checkSyntax(String mulCommandIn)
        {
            ///<remarks>
            ///Trims the command and splits it into two parts, command and parameter
            ///</remarks>
            String[] inputs = mulCommandIn.Trim().Split();
            ///<remarks>
            ///Makes command case insensitive
            ///</remarks>
            inputs[0] = inputs[0].ToLower();
            if (keywords.Contains(inputs[0]))
            {
                if (inputs[0].Equals(keywords[0]))
                {
                    ///<remarks>
                    ///Declares a variable
                    ///</remarks>
                    return declareVariable(inputs);    
                }
                else if (inputs[0].Equals(keywords[4]) || inputs[0].Equals(keywords[5]))
                {
                    return if_reader(inputs);
                }
                else if (inputs[0].Equals(keywords[1]) || inputs[0].Equals(keywords[2]))
                {
                    return loop_reader(inputs);
                }else if (inputs[0].Equals(keywords[7]) || inputs[0].Equals(keywords[8]))
                {
                    return declareMethod(inputs);
                }
            }
            else if (varDict.ContainsKey(inputs[0]))
            {
                ///<remarks>
                ///Updates the value in the variable
                ///</remarks>
                return updateVariable(inputs);  
            }
            else if (methodDict.ContainsKey(inputs[0]))
            {
                //check only the syntax
                //return method call and method name
                //method name ma check syntax loop lagayera
                //execute
                //good luck!
                if (inputs.Length == 4 && inputs[1] == "(" && inputs[3] == ")")
                {
                    String[] parameter = inputs[2].Split(',');
                    String parameterList="";
                    if (parameter[parameter.Length - 1] == "")
                    {
                        MessageBox.Show("Parameter Error. Method not declared", "Method Declaration error");
                        return "error";
                    }
                    else
                    {
                        try
                        {
                            System.Convert.ToInt32(parameter[0]);
                            parameterList = parameter[0];
                        }
                        catch (FormatException)
                        {
                            if (varDict.ContainsKey(parameter[0]))
                            {
                                parameterList =""+varDict[parameter[0]];
                            }
                            else
                            {
                                MessageBox.Show("Parameter Error. Method not called", "Parameter Format error");
                                return "error";
                            }
                        }
                        if (parameter.Length > 1)
                        {
                            for (int i = 1; i < parameter.Length; i++)
                            {
                                try
                                {
                                    int item = System.Convert.ToInt32(parameter[i]);
                                    parameterList = parameterList + "," + item;
                                }
                                catch (FormatException)
                                {
                                    if (varDict.ContainsKey(parameter[i]))
                                    {
                                        parameterList = parameterList + "," + varDict[parameter[i]];
                                    }
                                    else
                                    {
                                        MessageBox.Show("Parameter Error. Method not called", "Parameter Format error");
                                        return "error";
                                    }
                                }
                            }
                        }
                        return ("method-call " + inputs[0] + " "+ parameterList);
                    }
                }
                else if (inputs.Length == 2 && inputs[1] == "()")
                {
                    return ("method-call "+inputs[0]+" none");
                }
                else
                {
                    return "error";
                }

            }
            else if (commands.Contains(inputs[0]))
            {
                return singleLineReader(mulCommandIn);
            }
            else
            {
                MessageBox.Show(inputs[0] + "Keyword not identified", "Invalid Keyword");
            }
            return "error";
        }
        /// <summary>
        /// This method declares a variable
        /// </summary>
        /// <param name="inputs"></param>
        public String declareVariable(String[] inputs)
        {
            if (inputs[0].Equals(keywords[0]) && inputs.Length == 4 && inputs[2] == "=")
            {
                ///<remarks>
                /// Checks the format of variable name
                ///</remarks>
                try
                {
                    System.Convert.ToInt32(inputs[1]);
                    MessageBox.Show("Variable can only consists of aplhanumeric characters.", "Variable Name Error");
                    return "error";
                }
                catch (FormatException)
                {
                    ///<remarks>
                    /// Checks the format of syntax
                    ///</remarks>
                    try
                    {
                        int varValue = System.Convert.ToInt32(inputs[3]);
                        // Adding key/value pairs  
                        // in the Dictionary
                        // Using Add() method
                        varDict.Add(inputs[1], varValue);
                        return "dec-var";

                    }
                    catch (FormatException)
                    {
                        if (varDict.ContainsKey(inputs[3]))
                        {
                            int varValue = varDict[inputs[3]];
                            varDict.Add(inputs[1], varValue);
                            return "dec-var";
                        }
                        else
                        {
                            MessageBox.Show("Integer Value Required.", "Variable Error");
                            return "error";
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Invalid Syntax Format", "Error Declaring Variable");
                return "error";
            }
        }

        /// <summary>
        /// This method updates the value of existing variable
        /// </summary>
        /// <param name="inputs"></param>
        public String updateVariable(String[] inputs)
        {
            int updated_value ;
            if(inputs.Length != 3) {
                return "error";
            }
            else if (inputs[1] == "+" && inputs.Length == 3)
            {
                try
                {
                    updated_value = varDict[inputs[0]] + System.Convert.ToInt32(inputs[2]);
                    if (!loop || !method)
                        varDict[inputs[0]] = updated_value;
                    return ("var-update "+inputs[0]+" = "+ updated_value);
                }
                catch (FormatException)
                {
                    if (varDict.ContainsKey(inputs[2]))
                    {
                        updated_value = varDict[inputs[0]] + varDict[inputs[2]];
                        if (!loop || !method)
                            varDict[inputs[0]] = updated_value;
                        return ("var-update " + inputs[0] + " = " + updated_value);
                    }
                    else
                    {
                        MessageBox.Show("Integer Value Required.", "Variable Error");
                        return "error";
                    }
                }
            }
            else if (inputs[1] == "-" && inputs.Length == 3)
            {
                try
                {
                    Console.WriteLine("Before update:"+ varDict[inputs[0]]);
                    Console.WriteLine("update -:" + inputs[2]);
                    updated_value = varDict[inputs[0]] - System.Convert.ToInt32(inputs[2]); ;
                    if (!loop || !method)
                        varDict[inputs[0]] = updated_value;
                    Console.WriteLine("After update:" + updated_value);
                    return ("var-update " + inputs[0] + " = " + updated_value);
                }
                catch (FormatException)
                {
                    if (varDict.ContainsKey(inputs[2]))
                    {
                        Console.WriteLine("Before update:" + varDict[inputs[0]]);
                        Console.WriteLine("update -:" + inputs[2]);
                        updated_value = varDict[inputs[0]] - varDict[inputs[2]];
                        if (!loop || !method)
                            varDict[inputs[0]] = updated_value;
                        Console.WriteLine("After update:" + updated_value);
                        return ("var-update " + inputs[0] + " = " + updated_value);
                    }
                    else
                    {
                        return "error";
                    }
                }
            }
            else if (inputs[1] == "=" && inputs.Length == 3)
            {
                try
                {
                    updated_value = System.Convert.ToInt32(inputs[2]); ;
                    if (!loop || !method)
                        varDict[inputs[0]] = updated_value;
                    return ("var-update " + inputs[0] + " = " + updated_value);
                }
                catch (FormatException)
                {
                    if (varDict.ContainsKey(inputs[2]))
                    {
                        updated_value = varDict[inputs[2]];
                        if (!loop || !method)
                            varDict[inputs[0]] = updated_value;
                        return ("var-update " + inputs[0] + " = " + updated_value);
                    }
                    else
                    {
                        return "error";
                    }
                }

            }
            return "error";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        public String if_reader(String[] inputs)
        {
            if (inputs[0].Equals(keywords[4]) && inputs.Length == 4 &&  (inputs[2] == "=" || inputs[2] == ">" || inputs[2] == "<" || inputs[2] == ">=" || inputs[2] == "<=") && varDict.ContainsKey(inputs[1]))
            {
                ifcon = true;
                try
                {
                    int variable = System.Convert.ToInt32(inputs[3]);
                    if (inputs[2] == "=" && variable == varDict[inputs[1]])
                    {
                        ifcon = true;
                        return "if-true";
                    }
                    else if (inputs[2] == ">" && (varDict[inputs[1]] > variable))
                    {
                        ifcon = true;
                        return "if-true";
                    }
                    else if (inputs[2] == "<" && (varDict[inputs[1]] < variable))
                    {
                        ifcon = true;
                        return "if-true";
                    }
                    else if (inputs[2] == ">=" && (varDict[inputs[1]] >= variable))
                    {
                        ifcon = true;
                        return "if-true";
                    }
                    else if (inputs[2] == "<=" && (varDict[inputs[1]] <= variable))
                    {
                        ifcon = true;
                        return "if-true";
                    }
                    else
                    {
                        return "if-false";
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Variable Format Error", "Variable Condition Error");
                    return "if-error";
                }

            }
            else if (inputs[0].Equals(keywords[5]) && inputs.Length == 1)
            {
               if (ifcon)
                {
                    ifcon = false;
                    return "mul-if";
                }
                else
                {
                    MessageBox.Show("Endif without if", "If error");
                    return "error";
                }    
            }
            else if (inputs[0].Equals(keywords[4]) && inputs.Length > 5 && inputs[4].ToLower().Equals(keywords[6]) && (inputs[2] == "=" || inputs[2] == ">" || inputs[2] == "<") && varDict.ContainsKey(inputs[1]))
            {
                String command = "";
                for (int j = 5; j< inputs.Length; j++)
                {
                    command = command + inputs[j] + " ";
                }
                try
                {
                    int variable = System.Convert.ToInt32(inputs[3]);
                    if (inputs[2] == "=" && variable == varDict[inputs[1]])
                    {
                        ifcon = true;
                        return singleLineReader(command);
                    }
                    else if (inputs[2] == ">" && varDict[inputs[1]] > variable)
                    {
                        ifcon = true;
                        return singleLineReader(command);
                    }
                    else if (inputs[2] == "<" && varDict[inputs[1]] < variable)
                    {
                        ifcon = true;
                        return singleLineReader(command);
                    }
                    else if (inputs[2] == ">=" && varDict[inputs[1]] >= variable)
                    {
                        ifcon = true;
                        return singleLineReader(command);
                    }
                    else if (inputs[2] == "<=" && varDict[inputs[1]] <= variable)
                    {
                        ifcon = true;
                        return singleLineReader(command);
                    }
                    else
                    {
                        return "false-if";
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Variable Format Error", "Variable Condition Error");
                    return "if-error";
                }

            }
            else
            {
                return "error";
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        public String loop_reader(String[] inputs)
        {
            
            if (inputs[0].Equals(keywords[1]) && inputs.Length == 5 && inputs[1].Equals(keywords[3]) && (inputs[3] == ">=" || inputs[3] == "<=") && varDict.ContainsKey(inputs[2]))
            {
                looplimitkey = inputs[2];
                int initiation = varDict[inputs[2]];
                try
                {
                    looplimit = System.Convert.ToInt32(inputs[4]);
                }
                catch (FormatException)
                {
                    if (varDict.ContainsKey(inputs[2]))
                    {
                        looplimit = varDict[inputs[4]];
                    }
                    else
                    {
                        MessageBox.Show(inputs[4]+" is not a number", "Loop Error");
                        return "loop-error";
                    }
                }
                if(inputs[3] == ">=" && varDict[inputs[2]] >= looplimit)
                {
                    loop = true;
                    return ("loop-true >= "+ inputs[2]+" "+ initiation);
                }else if (inputs[3] == "<=" && varDict[inputs[2]] <= looplimit)
                {
                    loop = true;
                    return ("loop-true <= " + inputs[2] + " " + initiation);
                }
                else
                {
                    loop = true;
                    return ("loop-false");
                }

            }
            else if (inputs[0].Equals(keywords[2]) && inputs.Length == 1)
            {
                if (loop)
                {
                    loop = false;
                    return "end-loop";
                }
                else
                {
                    MessageBox.Show("Endloop without loop", "Loop error");
                    return "error";
                }
            }
                return "error";
        }

        public String declareMethod(String[] inputs)
        {
            if (inputs[0].Equals(keywords[7]) && inputs.Length == 5 && inputs[2]=="(" && inputs[4] == ")")
            {
                String[] parameter = inputs[3].Split(',');
                if (parameter[parameter.Length - 1] == "")
                {
                    MessageBox.Show("Parameter Error. Method not declared", "Method Declaration error");
                    return "error";
                }
                else
                {
                    try {
                        methodDict.Add(inputs[1], inputs[3]);
                        method = true;
                        return "method-dec " + inputs[1];
                    }
                    catch (ArgumentException)
                    {
                        MessageBox.Show("Method with same name already exists", "Method Declaration error");
                        return "error";
                    }
                }
            }else if (inputs[0].Equals(keywords[7]) && inputs.Length == 3 && inputs[2] == "()")
            {
                try
                {
                    methodDict.Add(inputs[1], "none");
                    method = true;
                    return "method-dec " + inputs[1];
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Method with same name already exists", "Method Declaration error");
                    return "error";
                }
            }
            else if (inputs[0].Equals(keywords[8]) && inputs.Length == 1)
            {
                if (method)
                {
                    method = false;
                    return "method-end";
                }
                else
                {
                    MessageBox.Show("endmethod without method", "Method Declaration error");
                }
            }
            else
            {
                MessageBox.Show("Syntax Error. Method not declared", "Method Declaration error");
            }
            return "error";
        }

        /// <summary>
        /// This method reads commands on command line one at a time and validates it
        /// </summary>
        /// <param name="commandIn"></param>
        /// <returns></returns>
        public String singleLineReader(String commandIn)
        {
            ///<remarks>
            ///Trims the command and splits it into two parts, command and parameter
            ///</remarks>
            String[] inputs = commandIn.Trim().Split();

            ///<remarks>
            ///Makes command case insensitive
            ///</remarks>
            inputs[0] = inputs[0].ToLower();
           
            if (keywords.Contains(inputs[0]))
            {
                if (inputs[0].Equals(keywords[0]))
                {
                    ///<remarks>
                    ///Declares a variable
                    ///</remarks>
                    declareVariable(inputs);
                }
            }
            else if (varDict.ContainsKey(inputs[0]))
            {
                ///<remarks>
                ///Updates the value in the variable
                ///</remarks>
                updateVariable(inputs);
            }
            ///<remarks>
            ///Validates commands and its parameters
            ///</remarks>
            else if (commands.Contains(inputs[0]) && inputs.Length == 2)
            {
                String[] parameters = inputs[1].Split(',');

                if (inputs[0].Equals(commands[0]) && parameters.Length != 2)
                {
                    parameterError("DrawTo", parameters.Length, 2);
                }
                else if (inputs[0].Equals(commands[0]) && parameters.Length == 2)
                {
                    ///<remarks>
                    /// Checks the format of parameter
                    ///</remarks>
                    try
                    {
                        System.Convert.ToInt32(parameters[0]);
                        System.Convert.ToInt32(parameters[1]);
                        return "drawto " + parameters[0] + " " + parameters[1];
                    }
                    catch (FormatException)
                    {
                        if (!method)
                        {
                            if (varDict.ContainsKey(parameters[0]) && varDict.ContainsKey(parameters[1]))
                            {
                                return "drawto " + varDict[parameters[0]] + " " + varDict[parameters[1]];
                            }
                            else
                                MessageBox.Show("Invalid Syntax Format", "Error");
                        }
                        else
                        {
                            return "ignored";
                        }
                    }
                }

                else if (inputs[0].Equals(commands[1]) && parameters.Length != 2)
                {
                    parameterError("MoveTo", parameters.Length, 2);
                }
                else if (inputs[0].Equals(commands[1]) && parameters.Length == 2)
                {
                    try
                    {
                        System.Convert.ToInt32(parameters[0]);
                        System.Convert.ToInt32(parameters[1]);
                        return "moveto " + parameters[0] + " " + parameters[1];
                    }
                    catch (FormatException)
                    {
                        if (!method)
                        {
                            if (varDict.ContainsKey(parameters[0]) && varDict.ContainsKey(parameters[1]))
                            {
                                return "moveto " + varDict[parameters[0]] + " " + varDict[parameters[1]];
                            }
                            else
                                MessageBox.Show("Invalid Syntax Format", "Error");
                        }
                        else
                        {
                            return "ignored";
                        }
                    }
                }

                else if (inputs[0].Equals(commands[2]) && parameters.Length != 1)
                {
                    parameterError("Circle", parameters.Length, 1);
                }
                else if (inputs[0].Equals(commands[2]) && parameters.Length == 1)
                {
                    try
                    {
                        System.Convert.ToInt32(parameters[0]);
                        return "circle " + parameters[0];
                    }
                    catch (FormatException e)
                    {
                        if (!method)
                        {
                            if (varDict.ContainsKey(parameters[0]))
                            {
                                return "circle " + varDict[parameters[0]];
                            }
                            else
                                MessageBox.Show("Invalid Syntax Format", "Error");
                        }
                        else
                        {
                            return "ignored";
                        }
                    }
                }
                else if (inputs[0].Equals(commands[3]) && parameters.Length != 2)
                {
                    parameterError("Rectangle", parameters.Length, 2);
                }
                else if (inputs[0].Equals(commands[3]) && parameters.Length == 2)
                {
                    try
                    {
                        Convert.ToInt32(parameters[0]);
                        Convert.ToInt32(parameters[1]);
                        return "rectangle " + parameters[0] + " " + parameters[1];
                    }
                    catch (FormatException)
                    {
                        if (!method)
                        {
                            if (varDict.ContainsKey(parameters[0]) && varDict.ContainsKey(parameters[1]))
                            {
                                return "rectangle " + varDict[parameters[0]] + " " + varDict[parameters[1]];
                            }
                            else
                                MessageBox.Show("Invalid Syntax Format", "Error");
                        }
                        else
                        {
                            return "ignored";
                        }
                    }
                }
                else if (inputs[0].Equals(commands[4]) && parameters.Length != 3)
                {
                    parameterError("Triangle", parameters.Length, 3);
                }
                else if (inputs[0].Equals(commands[4]) && parameters.Length == 3)
                {
                    try
                    {
                        Convert.ToInt32(parameters[0]);
                        Convert.ToInt32(parameters[1]);
                        Convert.ToInt32(parameters[2]);
                        return "triangle " + parameters[0] + " " + parameters[1] + " " + parameters[2];
                    }
                    catch (FormatException)
                    {
                        if (!method)
                        {
                            if (varDict.ContainsKey(parameters[0]) && varDict.ContainsKey(parameters[1]) && varDict.ContainsKey(parameters[2]))
                            {
                                return "triangle " + varDict[parameters[0]] + " " + varDict[parameters[1]] + " " + varDict[parameters[2]];
                            }
                            else
                                MessageBox.Show("Invalid Syntax Format", "Error");
                        }
                        else
                        {
                            return "ignored";
                        }
                    }
                }
            }
            else if (inputs[0]=="polygon" && inputs.Length > 3)
            {
                String returnvalue = "";
                for (int i = 1; i<inputs.Length; i++)
                {
                    String[] parameters = inputs[i].Split(',');
                    if (parameters.Length != 2)
                    {
                        parameterError("Polygon Points", parameters.Length, 2);
                        return "error";
                    }
                    else if (parameters.Length == 2)
                    {
                        ///<remarks>
                        /// Checks the format of parameter
                        ///</remarks>
                        try
                        {
                            System.Convert.ToInt32(parameters[0]);
                            System.Convert.ToInt32(parameters[1]);
                            returnvalue = returnvalue + parameters[0] + "," + parameters[1] + " ";
                        }
                        catch (FormatException)
                        {
                            if (!method)
                            {
                                if (varDict.ContainsKey(parameters[0]) && varDict.ContainsKey(parameters[1]))
                                {
                                    returnvalue = returnvalue + varDict[parameters[0]] + "," + varDict[parameters[1]] + " ";
                                }
                                else
                                {
                                    MessageBox.Show("Invalid Syntax Format", "Error");
                                    return "error";
                                }
                            }
                            else
                            {
                                return "ignored";
                            }
                            
                        }
                    }
                }
                return "polygon " + returnvalue;
            }
            else if (inputs[0] == "polygon" && inputs.Length < 4 )
            {
                MessageBox.Show(inputs[0] + "Polygon Requires more than 3 points.", "Polygon Error");
            }
            else
            {
                MessageBox.Show(inputs[0] + "Command not found.", "Syntax Error");
            }
            ///<remarks>
            ///if there are any error, this method returns error
            ///</remarks>
            return "error";
        }

        /// <summary>
        /// This method displays error if there are any syntax error regarding parameter
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="given"></param>
        /// <param name="required"></param>
        public void parameterError(String shape, int given, int required)
        {
            MessageBox.Show("Parameter Error. " + shape + " requires " + required + " parameters. " + given + " given.", "Syntax Error");

        }
    }
}

